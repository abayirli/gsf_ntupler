# GSF_Ntupler

NTupler package based on TRTNTupler (https://gitlab.cern.ch/ddavis/TRTNTupler)

Stores InDet and GSF track information for electrons.
